import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import Sentence from "./components/Sentence";
import logo from "./logo.svg";

const Logo = styled.img`
  max-width: 200px;
  margin-bottom: 3rem;
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 5rem;
  flex-direction: column;
  background-image: -webkit-linear-gradient(
    top left,
    #007d35 0%,
    #007d35 40%,
    #0f574e 100%
  );
  min-height: 100vh;
  min-width: 100vw;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

const Button = styled.button`
  background: -webkit-linear-gradient(
    top left,
    #007d35 0%,
    #007d35 40%,
    #007d35 100%
  );
  /* background-size: 300px; */
  font-family: Arial, Helvetica, sans-serif;
  color: #fff;
  margin-top: 3rem;
  padding: 1rem 1.5rem;
  font-size: 1rem;
  border: 2px solid black;
  transition: all 1s ease;

  :hover {
    cursor: pointer;
    background: -webkit-linear-gradient(
      top left,
      #007d35 0%,
      #1f9853 50%,
      #07582a 100%
    );
  }
`;

function App() {
  // Creacion del state
  const [phrase, setPhrase] = useState({});

  const APIconsult = async () => {
    const api = await fetch(
      "https://breaking-bad-quotes.herokuapp.com/v1/quotes"
    );
    const request = await api.json();
    setPhrase(request[0]);
  };

  // Cargar frase al inicio
  useEffect(() => {
    APIconsult();
  }, []);

  return (
    <Container>
      <Logo src={logo} alt="" />
      <Sentence phrase={phrase}></Sentence>
      <Button onClick={APIconsult}>
        <h1>Breaking Bad Sentences</h1>
      </Button>
    </Container>
  );
}

export default App;
