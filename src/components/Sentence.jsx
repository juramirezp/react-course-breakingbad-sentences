import React from "react";
import styled from "@emotion/styled";

const SentenseContainer = styled.div`
  padding: 3rem;
  border-radius: 0.5rem;
  background-color: #fff;
  max-width: 800px;

  h1 {
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    position: relative;
    padding-left: 3rem;
    &::before {
      content: open-quote;
      font-size: 8rem;
      color: black;
      position: absolute;
      left: -1rem;
      top: -2rem;
    }
  }
  p {
    font-family: Verdana, Geneva, Tahoma, sans-serif;
    font-size: 1.4rem;
    font-weight: bold;
    text-align: right;
    color: #666;
    margin-top: 2rem;
  }
`;

const Sentence = ({ phrase }) => {
  // Verificar si el objeto esta vacio
  if (Object.keys(phrase).length === 0) return null;

  return (
    <SentenseContainer>
      <h1>{phrase.quote}</h1>
      <p>-{phrase.author}</p>
    </SentenseContainer>
  );
};

export default Sentence;
